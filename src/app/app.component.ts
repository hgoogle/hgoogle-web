import {Component} from "@angular/core"
import {RouteConfig, RouterLink, Router} from "@angular/router-deprecated"
import {LoginComponent} from "./pages/login/login.component";
import {SignupComponent} from "./pages/signup/signup.component";
import {HomeComponent} from "./pages/home/home.component";


@Component({
  selector:"app",
  template: require('./app.html')
})

@RouteConfig([
  { path: '/', redirectTo: ['/Home'] },
  { path: '/home', component: HomeComponent, as: 'Home' },
  { path: '/login', component: LoginComponent, as: 'Login' },
  { path: '/signup', component: SignupComponent, as: 'Signup' },
])

export class App {
  
}